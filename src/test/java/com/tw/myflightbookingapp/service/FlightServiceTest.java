package com.tw.myflightbookingapp.service;

import com.tw.myflightbookingapp.model.Flight;
import com.tw.myflightbookingapp.repository.FlightBaseFareTable;
import com.tw.myflightbookingapp.repository.FlightRouteTable;
import com.tw.myflightbookingapp.repository.FlightSeatsTable;
import com.tw.myflightbookingapp.repository.FlightTable;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class FlightServiceTest {

    FlightTable flightTable=mock(FlightTable.class);

    FlightSeatsTable flightSeatsTable = new FlightSeatsTable();
    FlightRouteTable flightRouteTable =new FlightRouteTable();
    FlightBaseFareTable flightBaseFareTable =new FlightBaseFareTable();

    FlightService flightService = new FlightService(flightTable,flightSeatsTable,flightRouteTable,flightBaseFareTable);

    @Test
    public void shouldReturnAllFlights()
    {
        //flightTable = mock(FlightTable.class);
        List<Flight> flights = new ArrayList<>();

        flights.add(new Flight("G8-188", "GoAir", 100, 50, 50));
        flights.add(new Flight("G8-188", "GoAir", 100, 50, 50));
        flights.add(new Flight("6E-105", "IndigoAir", 75, 30, 20));
        flights.add(new Flight("G8-188", "GoAir", 100, 50, 50));
        flights.add(new Flight("6E-105", "IndigoAir", 75, 30, 20));
        flights.add(new Flight("G8-188", "GoAir", 100, 50, 50));
        flights.add(new Flight("6E-105", "IndigoAir", 75, 30, 20));
        flights.add(new Flight("G8-188", "GoAir", 100, 50, 50));
        flights.add(new Flight("AI-317 ", "Air India", 200, 50, 50));
        flights.add(new Flight("AI-603 ", "Air India", 200, 50, 50));

        when(flightTable.getAllFlights()).thenReturn(flights);
        Collection<Flight> actualFlights = flightService.getAllFlights();

        assertEquals(flights, actualFlights);

        //assertThat(actualFlights).hasSize()
    }


    @Test
    public void searchAllRoutes() {
    }

    @Test
    public void searchAllRoutesByDepartDate() {
    }

}