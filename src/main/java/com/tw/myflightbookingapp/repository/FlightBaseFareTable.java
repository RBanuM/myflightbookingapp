package com.tw.myflightbookingapp.repository;

import com.tw.myflightbookingapp.model.FlightBaseFare;
import com.tw.myflightbookingapp.model.TravelClass;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Component
public class FlightBaseFareTable {

    private List<FlightBaseFare> baseFares = initBaseFares();

    private List<FlightBaseFare> initBaseFares()
    {
        baseFares = new ArrayList<FlightBaseFare>();

        baseFares.add(new FlightBaseFare("DEL-BLR-11",13000F, 6000F, 20000F));
        baseFares.add(new FlightBaseFare("BLR-DEL-12",13000F, 6000F, 20000F));
        baseFares.add(new FlightBaseFare("DEL-BLR-13",13000F, 6000F, 20000F));
        baseFares.add(new FlightBaseFare("CHE-BLR-14",13000F, 6000F, 20000F));

        return baseFares;
    }

    public double getBaseFare(String routeId, TravelClass travelClass)
    {
        double dFare = 0.0;

        for (FlightBaseFare bfare: baseFares)
        {
            if (bfare.getRouteId().equals(routeId))
            {
                if (travelClass == TravelClass.BUSINESS_CLASS)
                    dFare = bfare.getBaseFareBusinessClass();
                else if (travelClass == TravelClass.FIRST_CLASS)
                    dFare = bfare.getBaseFareFirstClass();
                else if (travelClass == TravelClass.ECONOMY_CLASS)
                    dFare = bfare.getBaseFareEconomyClass();
            }
        }
        return dFare;
    }

}
