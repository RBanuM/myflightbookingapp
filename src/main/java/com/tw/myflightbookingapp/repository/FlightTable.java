package com.tw.myflightbookingapp.repository;


import com.tw.myflightbookingapp.model.Flight;
import com.tw.myflightbookingapp.model.TravelClass;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.ArrayList;

@Component
public class FlightTable {


    private List<Flight> flights  = initFlightTable();


    private List<Flight> initFlightTable()
    {
        flights = new ArrayList<Flight>();

        flights.add(new Flight("G8-188", "GoAir", 100, 50, 50));
        flights.add(new Flight("G8-188", "GoAir", 100, 50, 50));
        flights.add(new Flight("6E-105", "IndigoAir", 75, 30, 20));
        flights.add(new Flight("G8-188", "GoAir", 100, 50, 50));
        flights.add(new Flight("6E-105", "IndigoAir", 75, 30, 20));
        flights.add(new Flight("G8-188", "GoAir", 100, 50, 50));
        flights.add(new Flight("6E-105", "IndigoAir", 75, 30, 20));
        flights.add(new Flight("G8-188", "GoAir", 100, 50, 50));
        flights.add(new Flight("AI-317 ", "Air India", 200, 50, 50));
        flights.add(new Flight("AI-603 ", "Air India", 200, 50, 50));

        return flights;
    }

    public List<Flight> getAllFlights()
    {
        return flights;
    }

    public int getTotalSeats(String flightId, TravelClass tClass)
    {
        int noSeats = 0;
        for (Flight f: flights)
        {
            if (flightId.equals(f.getFlightId()))
            {
                if (tClass == TravelClass.ECONOMY_CLASS)
                    noSeats = f.getNoOfEconomySeats();
                else if (tClass == TravelClass.BUSINESS_CLASS)
                    noSeats = f.getNoOfBusinessSeats();
                else if (tClass == TravelClass.FIRST_CLASS)
                    noSeats = f.getNoOfFirstSeats();
            }
        }
        return noSeats;
    }
}