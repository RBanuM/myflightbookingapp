package com.tw.myflightbookingapp.repository;

import com.tw.myflightbookingapp.model.FlightSeats;
import com.tw.myflightbookingapp.model.TravelClass;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class FlightSeatsTable {

    private List<FlightSeats> flightSeats = initFlightSeats();

    private List<FlightSeats> initFlightSeats()
    {
        flightSeats = new ArrayList<FlightSeats>();

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

            flightSeats.add(new FlightSeats("DEL-BLR-11", sdf.parse("31-08-2019 10:20:56"), 30, 10, 100));
            flightSeats.add(new FlightSeats("BLR-DEL-12", sdf.parse("31-09-2019 10:20:56"), 30, 10, 100));
            flightSeats.add(new FlightSeats("DEL-BLR-13", sdf.parse("11-09-2019 10:20:56"),30, 10, 100));
            flightSeats.add(new FlightSeats("CHE-BLR-14", sdf.parse("21-09-2019 10:20:56"), 30, 10, 100));
        }
        catch (Exception e)
        {
            System.out.println("FlightRouteTable Exception" + e.getStackTrace());
        }
        return flightSeats;
    }

    public Collection<FlightSeats> hasSeats(String routeId, int passengerCount)
    {
        return flightSeats.stream().filter(
                seat -> seat.getRouteId().equals(routeId) &&
                        seat.getNoOfSeatsEconomyClass() >= passengerCount)
                .collect(Collectors.toList());
    }

    public int getDaysLeftForDeparture(Date departDate)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date();
        System.out.println(formatter.format(date));

        DateTime currentDateTime = new DateTime(date);
        DateTime departDateTime = new DateTime(departDate);

        int noDays = Days.daysBetween(currentDateTime, departDateTime).getDays();

        return noDays;
    }

    public Collection<FlightSeats> hasSeatsByDate(String routeId, int passengerCount, Date departDate)
    {
        int noDays = getDaysLeftForDeparture(departDate);

        //decide whether we have to include first class seats and business seats
        if (noDays <= 10)  //First Class: Opens 10 days before the departDate
        {
            return flightSeats.stream().filter(
                    seat -> seat.getRouteId().equals(routeId) &&
                            seat.getNoOfSeatsEconomyClass() >= passengerCount &&
                            seat.getNoOfSeatsFirstClass() >= passengerCount &&
                            seat.getNoOfSeatsBusinessClass() >= passengerCount &&
                            seat.getDepartureDate().equals(departDate))
                    .collect(Collectors.toList());
        }
        else if (noDays <= 30 && noDays > 10) //Business class: Opens 30 days before the departDate
        {
            return flightSeats.stream().filter(
                    seat -> seat.getRouteId().equals(routeId) &&
                            seat.getNoOfSeatsEconomyClass() >= passengerCount &&
                            seat.getNoOfSeatsBusinessClass() >= passengerCount &&
                            seat.getDepartureDate().equals(departDate))
                    .collect(Collectors.toList());
        }
        else
            {
            return flightSeats.stream().filter(
                    seat -> seat.getRouteId().equals(routeId) &&
                            seat.getNoOfSeatsEconomyClass() >= passengerCount &&
                            seat.getDepartureDate().equals(departDate))
                    .collect(Collectors.toList());
        }
    }



    public Collection<FlightSeats> hasSeatsByTravelClass(String routeId,
                                                         int passengerCount,
                                                         Date departDate,
                                                         TravelClass travelClass)
    {
        Collection<FlightSeats> fSeatsList = new ArrayList<>();

        switch (travelClass)
        {
            case BUSINESS_CLASS:
                fSeatsList = flightSeats.stream().filter(
                        seat -> seat.getRouteId().equals(routeId) &&
                                seat.getNoOfSeatsBusinessClass() >= passengerCount &&
                                seat.getDepartureDate().equals(departDate))
                        .collect(Collectors.toList());

            break;
            case FIRST_CLASS:
                fSeatsList = flightSeats.stream().filter(
                        seat -> seat.getRouteId().equals(routeId) &&
                                seat.getNoOfSeatsFirstClass() >= passengerCount &&
                                seat.getDepartureDate().equals(departDate))
                        .collect(Collectors.toList());
                break;
            case ECONOMY_CLASS:
                fSeatsList = flightSeats.stream().filter(
                        seat -> seat.getRouteId().equals(routeId) &&
                                seat.getNoOfSeatsEconomyClass() >= passengerCount &&
                                seat.getDepartureDate().equals(departDate))
                        .collect(Collectors.toList());
                break;
        }
        return fSeatsList;
    }

}



