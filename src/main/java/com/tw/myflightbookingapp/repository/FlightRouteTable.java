package com.tw.myflightbookingapp.repository;

import com.tw.myflightbookingapp.model.FlightRoute;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class FlightRouteTable {

    private List<FlightRoute> flightRoutes = initFlightRoutes();

    private List<FlightRoute> initFlightRoutes() {
        flightRoutes = new ArrayList<FlightRoute>();

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

            flightRoutes.add(new FlightRoute("DEL-BLR-11", "G8-188", "DEL", "BLR", sdf.parse("31-08-2019 10:20:56")));
            flightRoutes.add(new FlightRoute("BLR-DEL-12", "IN-210", "BLR", "DEL", sdf.parse("31-09-2019 10:20:56")));
            flightRoutes.add(new FlightRoute("DEL-BLR-13", "AI-510", "DEL", "BLR", sdf.parse("11-09-2019 10:20:56")));
            flightRoutes.add(new FlightRoute("CHE-BLR-14", "G8-188", "CHE", "BLR", sdf.parse("21-09-2019 10:20:56")));
        }
        catch (Exception e)
        {
            System.out.println("FlightRouteTable Exception" + e.getStackTrace());
        }
        return flightRoutes;
    }

    public Collection<FlightRoute> searchAllRoutes(String source, String destination)
    {
        return flightRoutes.stream()
                .filter(route -> route.getSrcLocation().equals(source) &&
                        route.getDestLocation().equals(destination))
                .collect(Collectors.toList());


    }

    public String getFlightId(String routeId)
    {
        String flightId = new String();
        for (FlightRoute fr: flightRoutes)
        {
            if (fr.getFlightId().equals(routeId))
                flightId = fr.getFlightId();
        }
        return flightId;
    }

}
