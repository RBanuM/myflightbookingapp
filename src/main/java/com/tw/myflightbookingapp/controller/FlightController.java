package com.tw.myflightbookingapp.controller;

import com.tw.myflightbookingapp.model.Flight;
import com.tw.myflightbookingapp.model.FlightRequestParam;
import com.tw.myflightbookingapp.model.ResponseResult;
import com.tw.myflightbookingapp.model.TravelClass;
import com.tw.myflightbookingapp.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Date;

@RestController
public class FlightController {

    private FlightService flightService;

    @Autowired
    public FlightController(FlightService flightService) {
        this.flightService = flightService;
    }

    //STORY 1
    @RequestMapping(value = "/flights", method = RequestMethod.POST)
    public ResponseEntity<Collection<ResponseResult>> searchAllRoutes(@RequestBody FlightRequestParam requestBody) throws Exception
    {
        String source = requestBody.getSource();
        String destination = requestBody.getDestination();
        int passengerCount = requestBody.getNoOfPassengers();
        String departDateString = requestBody.getDepartDate();
        String tClass = requestBody.getTravelClassStr();
        //TravelClass travelClass = requestBody.getTravelClass();

        /*
        SET THE DEFAULTS FOR THE ARGUMENTS
         */
        System.out.println("FROM /flights end point -- " + source + " " + destination + " " + passengerCount
                    + " " + " Departure Date: " + departDateString + " Travel Class " + tClass);

        //Collection<ResponseResult> result = flightService.searchAllRoutes(source, destination, passengerCount);
        Collection<ResponseResult> result = flightService.searchAllRoutesByTravelClass(source, destination, passengerCount,
                requestBody.getDepartureDate(departDateString), requestBody.getTravelClass(tClass));
        return ResponseEntity.ok(result);
    }

    //STORY 2
    @PostMapping("/flightsByDate")
    public Collection<ResponseResult> searchAllRoutesByDepartDate(@RequestParam(value = "source") String source,
                                                      @RequestParam(value = "destination") String destination,
                                                      @RequestParam(value = "noOfPassengers", defaultValue = "1") int passengerCount,
                                                      @RequestParam(value = "departDate") Date departDate)
    {
        return flightService.searchAllRoutesByDepartDate(source, destination, passengerCount, departDate);
    }

    //STORY 3
    @PostMapping("/flightsByTravelClass")
    public Collection<ResponseResult> searchAllRoutesByTravelClass(@RequestParam(value = "source") String source,
                                                                  @RequestParam(value = "destination") String destination,
                                                                  @RequestParam(value = "noOfPassengers", defaultValue = "1") int passengerCount,
                                                                  @RequestParam(value = "departDate") Date departDate,
                                                                   @RequestParam(value = "travelClass", defaultValue = "TravelClass.ECONOMY_CLASS") TravelClass travelClass)
    {

        return flightService.searchAllRoutesByTravelClass(source, destination, passengerCount, departDate, travelClass);
    }

}
