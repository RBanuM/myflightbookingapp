package com.tw.myflightbookingapp.model;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.lang.NonNull;

public class FlightBaseFare {
    private String routeId; //This is the primary key
    //private String flightId; //This is the foreign key //May be a duplicate here, probable candidate for deletion
    private double baseFareBusinessClass;
    private double baseFareFirstClass;
    private double baseFareEconomyClass;


    public FlightBaseFare(String routeId, double baseFareBusinessClass, double baseFareFirstClass, double baseFareEconomyClass) {
        this.routeId = routeId;
        this.baseFareBusinessClass = baseFareBusinessClass;
        this.baseFareFirstClass = baseFareFirstClass;
        this.baseFareEconomyClass = baseFareEconomyClass;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public double getBaseFareBusinessClass() {
        return baseFareBusinessClass;
    }

    public void setBaseFareBusinessClass(double baseFareBusinessClass) {
        this.baseFareBusinessClass = baseFareBusinessClass;
    }

    public double getBaseFareFirstClass() {
        return baseFareFirstClass;
    }

    public void setBaseFareFirstClass(double baseFareFirstClass) {
        this.baseFareFirstClass = baseFareFirstClass;
    }

    public double getBaseFareEconomyClass() {
        return baseFareEconomyClass;
    }

    public void setBaseFareEconomyClass(double baseFareEconomyClass) {
        this.baseFareEconomyClass = baseFareEconomyClass;
    }
}
