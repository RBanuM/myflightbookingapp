package com.tw.myflightbookingapp.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/*
 ** This class is different from FlightBaseFare class and is intentional
 ** This is because the FlightSeats table/class changes for every reservation whereas
 ** the FlightBaseFare table/class changes per route (one time change for a given route)
 ** may be periodical though...
 */
@NoArgsConstructor
@ToString
public class FlightSeats {
    private String routeId;
    private Date departureDate;
    private int noOfSeatsBusinessClass;  //Available seats in Business class
    private int noOfSeatsFirstClass;
    private int noOfSeatsEconomyClass;

    /*

     */
    public FlightSeats(String routeId, Date departureDate, int noOfSeatsBusinessClass, int noOfSeatsFirstClass, int noOfSeatsEconomyClass) {
        this.routeId = routeId;
        this.departureDate = departureDate;
        this.noOfSeatsBusinessClass = noOfSeatsBusinessClass;
        this.noOfSeatsFirstClass = noOfSeatsFirstClass;
        this.noOfSeatsEconomyClass = noOfSeatsEconomyClass;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public int getNoOfSeatsBusinessClass() {
        return noOfSeatsBusinessClass;
    }

    public void setNoOfSeatsBusinessClass(int noOfSeatsBusinessClass) {
        this.noOfSeatsBusinessClass = noOfSeatsBusinessClass;
    }

    public int getNoOfSeatsFirstClass() {
        return noOfSeatsFirstClass;
    }

    public void setNoOfSeatsFirstClass(int noOfSeatsFirstClass) {
        this.noOfSeatsFirstClass = noOfSeatsFirstClass;
    }

    public int getNoOfSeatsEconomyClass() {
        return noOfSeatsEconomyClass;
    }

    public void setNoOfSeatsEconomyClass(int noOfSeatsEconomyClass) {
        this.noOfSeatsEconomyClass = noOfSeatsEconomyClass;
    }
}
