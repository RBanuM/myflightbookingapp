package com.tw.myflightbookingapp.model;

    public enum TravelClass {
        BUSINESS_CLASS,
        FIRST_CLASS,
        ECONOMY_CLASS
    }

