package com.tw.myflightbookingapp.model;

import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FlightRequestParam {
    private String source;
    private String destination;
    private int noOfPassengers;
    private String departDate;
    private String travelClassStr;

    private TravelClass travelClass;
    private Date departureDate;


    public String getTravelClassStr() {
        return travelClassStr;
    }

    public void setTravelClassStr(String travelClassStr) {
        this.travelClassStr = travelClassStr;
    }



    public FlightRequestParam(String source, String destination, int noOfPassengers, String departDate, String travelClassStr) {
        this.source = source;
        this.destination = destination;
        this.noOfPassengers = noOfPassengers;
        this.departDate = departDate;
        this.travelClassStr = travelClassStr;

        System.out.println("FlightRequestParam travelInputClass : " + travelClassStr);
        System.out.println("depart Date : " + departDate);
    }


    public TravelClass getTravelClass(String travelClassStr)
    {

        if (travelClassStr.equals("ECONOMY_CLASS"))
            this.travelClass = TravelClass.ECONOMY_CLASS;
        else if (travelClassStr.equals("BUSINESS_CLASS"))
            this.travelClass = TravelClass.BUSINESS_CLASS;
        else if (travelClassStr.equals("FIRST_CLASS"))
            this.travelClass = TravelClass.FIRST_CLASS;
        System.out.println("Travel CLASS = " + this.travelClass);

        return this.travelClass;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getNoOfPassengers() {
        return noOfPassengers;
    }

    public void setNoOfPassengers(int noOfPassengers) {
        this.noOfPassengers = noOfPassengers;
    }

    public Date getDepartureDate(String departDate) throws Exception{

        Date date1=new SimpleDateFormat("dd-MM-yyyy").parse(departDate);
        System.out.println(departDate+"\t"+date1);
        this.departureDate = date1;
        return date1;
    }

    public String getDepartDate()
    {
        return this.departDate;
    }

    public void setDepartureDate(Date departureDateDate)
    {
        this.departureDate = departureDate;
    }

    public void setDepartDate(String departDate)
    {
        this.departDate = departDate;
    }

    public void setTravelClass(TravelClass travelClass) {
        this.travelClass = travelClass;
    }
}
