package com.tw.myflightbookingapp.model;

import java.util.Date;

public class ResponseResult {
    private String responseId;
    private String flightId;
    private String routeId;
    private String fightName;
    private String source;
    private String destination;
    private int noOfPassengers;
    private int noOfEconomySeats;
    private int noOfBusinessSeats;
    private int noOfFirstSeats;
    private Date departDate;
    private double totalFareEconomy;
    private double totalFareBusiness;
    private double totalFareFirst;

    public int getNoOfEconomySeats() {
        return noOfEconomySeats;
    }

    public void setNoOfEconomySeats(int noOfEconomySeats) {
        this.noOfEconomySeats = noOfEconomySeats;
    }

    public int getNoOfBusinessSeats() {
        return noOfBusinessSeats;
    }

    public void setNoOfBusinessSeats(int noOfBusinessSeats) {
        this.noOfBusinessSeats = noOfBusinessSeats;
    }

    public int getNoOfFirstSeats() {
        return noOfFirstSeats;
    }

    public void setNoOfFirstSeats(int noOfFirstSeats) {
        this.noOfFirstSeats = noOfFirstSeats;
    }

    public ResponseResult() { }

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getFightName() {
        return fightName;
    }

    public void setFightName(String fightName) {
        this.fightName = fightName;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getNoOfPassengers() {
        return noOfPassengers;
    }

    public void setNoOfPassengers(int noOfPassengers) {
        this.noOfPassengers = noOfPassengers;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public Date getDepartDate() {
        return departDate;
    }

    public void setDepartDate(Date departDate) {
        this.departDate = departDate;
    }

    public double getTotalFareEconomy() {
        return totalFareEconomy;
    }

    public void setTotalFareEconomy(double totalFareEconomy) {
        this.totalFareEconomy = totalFareEconomy;
    }

    public double getTotalFareBusiness() {
        return totalFareBusiness;
    }

    public void setTotalFareBusiness(double totalFareBusiness) {
        this.totalFareBusiness = totalFareBusiness;
    }

    public double getTotalFareFirst() {
        return totalFareFirst;
    }

    public void setTotalFareFirst(double totalFareFirst) {
        this.totalFareFirst = totalFareFirst;
    }
}
