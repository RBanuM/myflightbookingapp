package com.tw.myflightbookingapp.model;

import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@NoArgsConstructor
@ToString
public class FlightRoute {
    private String routeId;   //primary key
    private String flightId;  //foreign key from Flight table
    private String srcLocation;
    private String destLocation;
    //private Date departureDate;

    public FlightRoute(String routeId, String flightId, String srcLocation, String destLocation, Date departureDate) {
        this.routeId = routeId;
        this.flightId = flightId;
        this.srcLocation = srcLocation;
        this.destLocation = destLocation;
        //this.departureDate = departureDate;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getSrcLocation() {
        return srcLocation;
    }

    public void setSrcLocation(String srcLocation) {
        this.srcLocation = srcLocation;
    }

    public String getDestLocation() {
        return destLocation;
    }

    public void setDestLocation(String destLocation) {
        this.destLocation = destLocation;
    }

//    public Date getDepartureDate() {
//        return departureDate;
//    }
//
//    public void setDepartureDate(Date departureDate) {
//        this.departureDate = departureDate;
//    }
}
