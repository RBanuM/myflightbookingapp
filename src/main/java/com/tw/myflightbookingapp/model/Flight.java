package com.tw.myflightbookingapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

//All these values are fixed for a given flight or aeroplane (including the seat layout which decides the seat info below)
public class Flight {

    private String flightId;
    private String airlineName;
    private int noOfEconomySeats;
    private int noOfBusinessSeats;
    private int noOfFirstSeats;


    public Flight(String flightId, String airlineName, int noOfEconomySeats, int noOfBusinessSeats, int noOfFirstSeats) {
        this.flightId = flightId;
        this.airlineName = airlineName;
        this.noOfEconomySeats = noOfEconomySeats;
        this.noOfBusinessSeats = noOfBusinessSeats;
        this.noOfFirstSeats = noOfFirstSeats;
    }

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public int getNoOfEconomySeats() {
        return noOfEconomySeats;
    }

    public void setNoOfEconomySeats(int noOfEconomySeats) {
        this.noOfEconomySeats = noOfEconomySeats;
    }

    public int getNoOfBusinessSeats() {
        return noOfBusinessSeats;
    }

    public void setNoOfBusinessSeats(int noOfBusinessSeats) {
        this.noOfBusinessSeats = noOfBusinessSeats;
    }

    public int getNoOfFirstSeats() {
        return noOfFirstSeats;
    }

    public void setNoOfFirstSeats(int noOfFirstSeats) {
        this.noOfFirstSeats = noOfFirstSeats;
    }
}
