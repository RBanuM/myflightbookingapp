package com.tw.myflightbookingapp.service;

import com.tw.myflightbookingapp.model.*;
import com.tw.myflightbookingapp.repository.FlightBaseFareTable;
import com.tw.myflightbookingapp.repository.FlightRouteTable;
import com.tw.myflightbookingapp.repository.FlightSeatsTable;
import com.tw.myflightbookingapp.repository.FlightTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;


@Service
public class FlightService {

    //@Autowired
    private FlightTable flightTable;
    //@Autowired
    private FlightSeatsTable flightSeatsTable;
   // @Autowired
    private FlightRouteTable flightRouteTable;
   // @Autowired
    private FlightBaseFareTable flightBaseFareTable;
    private Collection<ResponseResult> resResult = new ArrayList<>();
    //private ResponseResult res = new ResponseResult();

    @Autowired
    public FlightService(FlightTable flightTable, FlightSeatsTable flightSeatsTable, FlightRouteTable flightRouteTable, FlightBaseFareTable flightBaseFareTable) {
        this.flightTable = flightTable;
        this.flightSeatsTable = flightSeatsTable;
        this.flightRouteTable = flightRouteTable;
        this.flightBaseFareTable = flightBaseFareTable;
    }


    public Collection<Flight> getAllFlights()
    {
        return flightTable.getAllFlights();
    }

    public Collection<ResponseResult> searchAllRoutes(String source, String destination, int passengerCount)
    {
        Collection<FlightSeats> fSeats = new ArrayList<>();
        Collection<FlightSeats> finalFlightSeats = new ArrayList<>();
        Collection<FlightRoute> fRoutes = flightRouteTable.searchAllRoutes(source, destination);
        Collection<FlightBaseFare> fFares = new ArrayList<>();
        flightBaseFareTable = new FlightBaseFareTable();

        for (FlightRoute fRoute : fRoutes) {
            fSeats = flightSeatsTable.hasSeats(fRoute.getRouteId(), passengerCount); //We consider economy seats by default
            finalFlightSeats.addAll(fSeats);
        }

        System.out.println("\n *****RESULT OF STORY 1***** \n");
        for (FlightSeats f : finalFlightSeats)
        {
            //get the flight id and fill it in, to prepare the response object
            ResponseResult res = new ResponseResult();
            res.setResponseId("SEARCH_STORY_1");
            res.setRouteId(f.getRouteId());
            res.setSource(source);
            res.setDestination(destination);
            res.setNoOfEconomySeats(f.getNoOfSeatsEconomyClass());

            //Add it to the LIST
            resResult.add(res);

            System.out.println("ROUTE ID -"+ res.getRouteId()+" SOURCE - " + res.getSource() + " DESTINATION - " +
                    res.getDestination() + " -- NO OF SEATS - " + res.getNoOfEconomySeats());
        }
        return resResult;
    }


    public Collection<ResponseResult> searchAllRoutesByDepartDate(String source,
                                                                  String destination,
                                                                  int passengerCount,
                                                                  Date departDate)
    {
        Collection<FlightSeats> fSeats = new ArrayList<>();
        Collection<FlightSeats> finalFlightSeats = new ArrayList<>();
        Collection<FlightRoute> fRoutes = flightRouteTable.searchAllRoutes(source, destination);

        for (FlightRoute fRoute : fRoutes) {
            fSeats = flightSeatsTable.hasSeatsByDate(fRoute.getRouteId(), passengerCount, departDate);
            finalFlightSeats.addAll(fSeats);
        }

        System.out.println("\n*****RESULT OF STORY 2*****\n");
        for (FlightSeats f : finalFlightSeats)
        {
            //get the flight id and fill it in, to prepare the response object
            ResponseResult res = new ResponseResult();
            res.setResponseId("SEARCH_STORY_2");
            res.setRouteId(f.getRouteId());
            res.setSource(source);
            res.setDestination(destination);
            res.setNoOfPassengers(passengerCount);
            res.setDepartDate(departDate);
            res.setNoOfEconomySeats(f.getNoOfSeatsEconomyClass());
            res.setNoOfEconomySeats(f.getNoOfSeatsBusinessClass());
            res.setNoOfEconomySeats(f.getNoOfSeatsFirstClass());

            //Add it to the LIST
            resResult.add(res);

            System.out.println("ROUTE ID -"+ res.getRouteId()+" SOURCE - " + res.getSource() + " DESTINATION - " +
                    res.getDestination() + " DEPARTURE DATE -" + res.getDepartDate() +
                    "NO OF ECONOMY SEATS - " + res.getNoOfEconomySeats() +
                    "NO OF BUSINESS SEATS - " + res.getNoOfBusinessSeats() +
                    "NO OF FIRST CLASS SEATS - " + res.getNoOfFirstSeats());
        }
        return resResult;

    }


    public Collection<ResponseResult> searchAllRoutesByTravelClass(String source,
                                                                   String destination,
                                                                   int passengerCount,
                                                                   Date departDate,
                                                                   TravelClass travelClass) {
        //Case: Travel class is entered
        Collection<FlightSeats> fSeats = new ArrayList<>();
        Collection<FlightSeats> finalFlightSeats = new ArrayList<>();
        Collection<FlightSeats> finalFlightSeatsByTravelClass = new ArrayList<>();

        Collection<FlightRoute> fRoutes = flightRouteTable.searchAllRoutes(source, destination);

        for (FlightRoute fRoute : fRoutes) {
            fSeats = flightSeatsTable.hasSeatsByDate(fRoute.getRouteId(), passengerCount, departDate);
            finalFlightSeats.addAll(fSeats);
        }


        for (FlightSeats fs : finalFlightSeats) {
            fSeats = flightSeatsTable.hasSeatsByTravelClass(fs.getRouteId(), passengerCount, departDate, travelClass);
            finalFlightSeatsByTravelClass.addAll(fSeats);

        //Case: Travel class is not entered -- default is economy class in this case
        }


            for (FlightSeats ffSeat : finalFlightSeatsByTravelClass) {


                String rId = ffSeat.getRouteId();
                double totalEconomySeatsFare = computeEconomyFare(rId, ffSeat, passengerCount);
                double totalBusinessSeatsFare = computeBusinessFare(rId, ffSeat, passengerCount);
                double totalFirstSeatsFare = computeFirstFare(rId, ffSeat, passengerCount);

                ResponseResult res = new ResponseResult();
                res.setResponseId("SEARCH_STORY_5_6_7");
                res.setRouteId(ffSeat.getRouteId());
                res.setSource(source);
                res.setDestination(destination);
                res.setNoOfPassengers(passengerCount);
                res.setDepartDate(departDate);
                res.setNoOfEconomySeats(ffSeat.getNoOfSeatsEconomyClass());
                res.setNoOfBusinessSeats(ffSeat.getNoOfSeatsBusinessClass());
                res.setNoOfFirstSeats(ffSeat.getNoOfSeatsFirstClass());
                res.setTotalFareEconomy(totalEconomySeatsFare);

                resResult.add(res);
            }

        return resResult;
        }


        /*
         if seats fall in
         first 40% then -- base fare
         next 50% then -- 30% extra of base fare
         last 10% then -- 60% extra of base fare
        */

        public double computeEconomyFare(String rId, FlightSeats ffSeat, int passengerCount)
        {
            String flightId = flightRouteTable.getFlightId(rId);
            //fetch the seats occupied for the route -- ECONOMY CLASS
            int totalEconomySeatsPerFlight = flightTable.getTotalSeats(flightId, TravelClass.ECONOMY_CLASS);
            int totalBusinessSeatsPerFlight = flightTable.getTotalSeats(flightId, TravelClass.BUSINESS_CLASS);
            int totalFirstSeatsPerFlight = flightTable.getTotalSeats(flightId, TravelClass.FIRST_CLASS);

            /*
             FARE CALCULATION FOR THE ECONOMY CLASS
             */
            int firstFortyPercentEconomySeats = (totalEconomySeatsPerFlight * 40 ) / 100;
            int nextFiftyPercentEconomySeats = ((totalEconomySeatsPerFlight - firstFortyPercentEconomySeats) * 50 ) / 100;
            //int lastTenPercentEconomySeats = (totalEconomySeatsPerFlight - (firstFortyPercentEconomySeats + nextFiftyPercentEconomySeats)) * 10 /100;
            int lastTenPercentEconomySeats = totalEconomySeatsPerFlight * 10 /100;
            int currentEconomySeats = ffSeat.getNoOfSeatsEconomyClass();

            //get the base fare from the FlightBaseFare table
            double baseFare = flightBaseFareTable.getBaseFare(rId, TravelClass.ECONOMY_CLASS);
            double totalEconomySeatsFare = 0;

            //Now compute the ticket cost based on the filling rates of the seats
            if (currentEconomySeats <= firstFortyPercentEconomySeats)
            {
                //Apply base fare
                totalEconomySeatsFare = passengerCount * baseFare;
            }
            else if (currentEconomySeats > firstFortyPercentEconomySeats && currentEconomySeats <= (firstFortyPercentEconomySeats + nextFiftyPercentEconomySeats))
            {
                //Apply 30% extra on base fare
                totalEconomySeatsFare = passengerCount * (baseFare + (30 * baseFare)/100);
            }
            else if ((currentEconomySeats <= totalEconomySeatsPerFlight) && (currentEconomySeats > (totalEconomySeatsPerFlight - lastTenPercentEconomySeats)))
            {
                //Apply 60% extra on base fare
                totalEconomySeatsFare = passengerCount * (baseFare + (60 * baseFare) / 100);
            }

            return totalEconomySeatsFare;
        }

        /*
        BUsiness class will be charged 40% extra of the base price if the travel is on MONDAY, FRIDAY AND SUNDAY
         */
        public double computeBusinessFare(String rId, FlightSeats fSeat, int passengerCount)
        {
            double totalBusinessSeatFare = 0.0D;
            String flightId = flightRouteTable.getFlightId(rId);
            //fetch the seats occupied for the route -- BUSINESS CLASS
            int totalBusinessSeatsPerFlight = flightTable.getTotalSeats(flightId, TravelClass.BUSINESS_CLASS);

            //get the base fare from the FlightBaseFare table
            double baseFare = flightBaseFareTable.getBaseFare(rId, TravelClass.BUSINESS_CLASS);

            Date departDate = fSeat.getDepartureDate();
            //Check if this date happens to be a MONDAY, FRIDAY AND SUNDAY
            SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE"); // the day of the week spelled out completely
            System.out.println(simpleDateformat.format(departDate));
            String departDatesDay = simpleDateformat.format(departDate);

            if (departDatesDay.toUpperCase().equals("MONDAY") || departDatesDay.toUpperCase().equals("FRIDAY") ||
                    (departDatesDay.toUpperCase().equals("SUNDAY")))
            {
                totalBusinessSeatFare = passengerCount * (baseFare + (40 * baseFare /100));
            }
            return totalBusinessSeatFare;

        }

        public double computeFirstFare(String rId, FlightSeats fSeat, int passengerCount)
        {
            Date departDate = fSeat.getDepartureDate();
            int noDays = flightSeatsTable.getDaysLeftForDeparture(departDate);
            //get the base fare from the FlightBaseFare table
            double baseFare = flightBaseFareTable.getBaseFare(rId, TravelClass.FIRST_CLASS);

            //decide the percent increase based on the days left
            /*
            if same day = 100% extra
            if 1 day left = 90% extra
            if 2 days left = 80% extra
            if 3 days left = 70% extra and so on
            if 9 days left = 10% extra
             */
            int percentIncrease = (10 - noDays) * 10;

            double totalFirstSeatFare = passengerCount * (baseFare + (baseFare * percentIncrease / 100));

            return totalFirstSeatFare;

        }
}

