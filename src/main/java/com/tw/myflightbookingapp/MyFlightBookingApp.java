package com.tw.myflightbookingapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class MyFlightBookingApp {

    public static void main(String[] args)
    {
        ConfigurableApplicationContext context = SpringApplication.run(MyFlightBookingApp.class, args);

    }
}
